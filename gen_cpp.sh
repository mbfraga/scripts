#! /bin/sh

_script_usage()
{
   echo "script usage: $(basename $0) [-l \"lib1,lib2\"] [-h] -- project name"
   exit 1
}

LIBRARIES=""

while getopts 'l:h' OPTION;
do case "$OPTION" in
   l)
      LIBRARIES="$LIBRARIES,$OPTARG"
      ;;
   h)
      _script_usage
      ;;
   [?])
      _script_usage
      ;;
   esac
done

shift "$(($OPTIND-1))"

if [ -z $@ ]; then
   echo "Expected project name..."
   _script_usage
   exit 1;
fi

IFS="$OLDIFS"

PROJECT_NAME=$@
CMAKE_LOCATION="$PROJECT_NAME/CMakeLists.txt"
CMAKE_VERSION="3.16"
CPP_NAME="main.cpp"
CPP_LOCATION="$PROJECT_NAME/$CPP_NAME"

if [ -e $PROJECT_NAME ]; then
   echo "'$PROJECT_NAME' exists..."
   exit 1;
fi

mkdir $PROJECT_NAME

if [ $? -ne 0 ]; then
   echo "Failed to create '$PROJECT_NAME'..."
   exit 1;
fi

_append_to_cmake()
{
   echo "$1" >> $CMAKE_LOCATION
}

_append_to_cpp()
{
   echo "$1" >> $CPP_LOCATION
}


_add_header()
{
   _append_to_cmake "cmake_minimum_required(VERSION $CMAKE_VERSION)\n"
   _append_to_cmake "project($PROJECT_NAME LANGUAGES CXX)\n"
   _append_to_cmake "# Settings\n"
   _append_to_cmake "set(CMAKE_CXX_STANDARD 17)"
   _append_to_cmake "set(CMAKE_CXX_STANDARD_REQUIRED ON)\n"
}

_add_project()
{
   _append_to_cmake "add_executable($PROJECT_NAME $CPP_NAME)\n"
}

_find_libraries()
{
   OLDIFS=$IFS
   IFS=","
   for lib in $LIBRARIES; do
      if [ ! -z $lib ]; then
         _append_to_cmake "find_package($lib REQUIRED)"
      fi
   done
}

_link_generic()
{
   _append_to_cmake "\ntarget_link_libraries($PROJECT_NAME PRIVATE $lib)"
}

_link_glm()
{
   _append_to_cmake "\ntarget_link_libraries($PROJECT_NAME PRIVATE glm::glm)"
}

_link_libraries()
{
   OLDIFS=$IFS
   IFS=","
   for lib in $LIBRARIES; do
      if [ ! -z $lib ]; then
         case $lib in
            "glm")
               _link_glm $glm
               ;;
            *)
               _link_generic $lib
               ;;
         esac

      fi
   done
   _append_to_cmake ""
}

_write_cpp_main()
{
   _append_to_cpp "#include <iostream>"
   _append_to_cpp ""
   _append_to_cpp "int main()"
   _append_to_cpp "{"
   _append_to_cpp "  std::cout << \"Hello, world!\" << std::endl;"
   _append_to_cpp "}"
}

touch $CMAKE_LOCATION
touch $CPP_LOCATION

_add_header

_find_libraries

_add_project

_link_libraries

_write_cpp_main

echo $PROJECT_NAME
